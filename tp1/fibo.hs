import System.Environment

fibo :: Integer -> Int

fibo x = fibo (x - 1) + fibo (x - 2)

main:: IO()

main = do
    calc <- getArgs
    fibo (toBin(calc))
