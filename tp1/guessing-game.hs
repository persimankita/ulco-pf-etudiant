import System.Random

guess :: Int -> Int -> IO ()
guess _ 0 = putStrLn "You lose!"
guess secret tries = do
    putStr ("Type a number"++(tries)++" : ")
    nstr <- getLine
    let n = read nstr
    if n == secret
        then putStrLn "You win!"
        else do
            putStrLn (if n < secret then "Too small!" else "Too big!")
            guess secret (tries - 1) 

main :: IO ()
main = do
    s <- randomRIO (0, 100)
    guess s 10