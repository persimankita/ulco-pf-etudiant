main:: IO()

main = do
    putStrLn "What's your name ?"
    nom <- getLine
    if nom == ""
        then putStrLn "Goodbye!"
        else do
            putStrLn("Hello " ++ nom ++ "!")
            main

       