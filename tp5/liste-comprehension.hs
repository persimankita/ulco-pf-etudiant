----------------------------------------------------------------------------
{-
Map multiplication par 2 avec liste de conpréhension
-}
----------------------------------------------------------------------------
doubler :: (a -> b) -> [a] ->[b]
doubler a l = [a x | x <- l]
----------------------------------------------------------------------------
{-
Fonction qui ne retient que les entiers pairs d'une liste de compréhension
-}
----------------------------------------------------------------------------
pairs:: Integral a => [a] -> [a]
pairs l =[x | x<- l, even x]
----------------------------------------------------------------------------
{-
Fonction permettant d'afficher des multiples d'un entier passé en paramètre
-}
----------------------------------------------------------------------------
multiples:: Int -> [Int]
multiples a = [ x | x <- [1,a]]
---------------------------------------------------------------------------
{-
Fonction principale
-}
----------------------------------------------------------

main :: IO()

main = do
--Multiplication par 2 de la liste via les listes de comprehension
    putStrLn "doubler *2 [1..4]"
    print(doubler (*2) [1..4])
--Selection des entiers pairs de la liste
    putStrLn "pairs [1..10]"
    print(pairs [1..10])
-- Afichage des multiples
    putStrLn "multiple 35"
    print(multiples 35)
