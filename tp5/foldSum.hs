------------------------------------------------------------------------------
{-
Fonction permettant d'additionner les éléments d'une liste                   |
-}
------------------------------------------------------------------------------
foldSum1:: [Int]->Int
foldSum1 [] = 0
foldSum1 [a] = a
foldSum1 (x:xs) = x + foldSum1 xs
------------------------------------------------------------------------------
{-
Même fonction avec foldl
-}
------------------------------------------------------------------------------
foldSum2:: Num a => [a] -> a
foldSum2 = foldr (+) 0
------------------------------------------------------------------------------
{-Même fonction avec fold recursive-}
------------------------------------------------------------------------------
myfoldl:: Num a => [a] -> a
myfoldl [] = 0
myfoldl [a] = a
myfoldl (x:xs) = foldr (+) x xs
------------------------------------------------------------------------------
{-
Fonction principale                                                          |
-}
------------------------------------------------------------------------------
main :: IO()
main = do
------------------------------------------------------------------------------
    putStrLn "foldSum [1..4]"
    print(foldSum1 [1..4])

------------------------------------------------------------------------------
    putStrLn "foldSum2 [1..5]"
    print(foldSum2 [1..5])
------------------------------------------------------------------------------
    putStrLn "myfoldl [1..7]"
    print(myfoldl [1..7])

