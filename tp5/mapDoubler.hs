import Data.Char --Pour manipuler les lettres
--------------------------------------------------------
{-
Fonction permettant de multiplier tous les éléments     |
d'une liste par 2
-}
--------------------------------------------------------
mapDoubler1 :: [Int] -> [Int]
mapDoubler1 [] = []
mapDoubler1 [x] = [2*x]
mapDoubler1 (x:xs) = 2 * x : mapDoubler1 xs 

-------------------------------------------------------
{-
Même fonction mais qui utilise le mapping              |
-}
-------------------------------------------------------

--mapDoubler2::Num a => [a] -> [a]
--mapDoubler2  b = map (*b)
-------------------------------------------------------
{-
Même fonction mais qui utilise le mapping récursif     |
-}
-------------------------------------------------------
mymap :: (a -> b) -> [a] ->[b]
mymap a [] = []
mymap a (x:xs) = a x : mymap a xs



--mymap a (x:xs) = (toUpper a) x : mymap a xs

-------------------------------------------------------
{-
Fonction principale                                     |
-}
-------------------------------------------------------
main:: IO()

main = do
    putStrLn "map Via liste de compréhension de la liste [1..4]"
    print(mymapComp (*2) [1..4])
----------------------------------------------------
    putStrLn "mapDoubler1 [1..5]"
    print(mapDoubler1 [1..5])
-------------------------------------------------------
   {- putStrLn "mapDoubler2 [1..5]"
    print(mapDoubler2 (*2) [1..5])-}
-------------------------------------------------------
    putStrLn "mymap [1..5]"
    print(mymap (*2) [1..5])
    putStrLn "mymap toto [1..5]"
    print(mymap (\ x -> "toto" ++ show  x) [1..5])