# ulco-pf-etudiant

dépôt étudiant du [module de PF](https://juliendehos.gitlab.io/posts/pf/index.html)
***
## Auteur

 Nom: MANKITA KOULOUBOUKA;
 
 Prénom: Persi Albry;
 
 Formation: L3 Informatique
***
### Réalisations :

* [TP 1](tp1) : [Découverte d’Haskell](https://juliendehos.gitlab.io/posts/pf/post21-tp1.html)

    * test du hello word en haskell
    * Documentation via [Hoogle](https://hoogle.haskell.org/) et [Hackage](https://hackage.haskell.org/)
    * Prise de connaissance de l'écriture desexpressions en haskell 
    * Système de typage
    * Fonctions
    * [Entrées-Sorties](tp1/io.hs)

- [TP 2](tp2): [Conditions, pattern matching](https://juliendehos.gitlab.io/posts/pf/post22-tp2.html)


- [TP 3](tp3): [Listes, tuples](https://juliendehos.gitlab.io/posts/pf/post23-tp3.html)

- [TP 4](tp4): [Fonctions récursives](https://juliendehos.gitlab.io/posts/pf/post24-tp4.html)
    * Fonctions récursives simple: [PGCD](tp4/pgcd.hs),[Factorielle](tp4/fact.hs),[Affichage en boucle](tp4/loopEcho.hs);

    * Récursivité Terminale:
    Factorielle terminale


- [TP 5](tp5): [Traitements de liste, listes en compréhension](https://juliendehos.gitlab.io/posts/pf/post25-tp5.html)
    * Implémentation de la [Map](tp5/mapDoubler.hs)
    * [Filtre](tp5/filterEven.hs) d'entiers pairs 

    * Calcul de la somme d'une liste avec les fonctions [fold](tp5/foldSum.hs)

    * Ajout des listes de compréhension pour les fonctions map et filter

    * Ajout de la fonction "multiples" qui retourne la liste des diviseurs du nombre passé en paramètre.
