
import System.Environment
cow :: String
cow = 
    "    \\   ^__^ \n\
    \     \\  (oo)\\_______ \n\
    \        (__)\\       )\\/\\ \n\
    \            ||----w | \n\
    \            ||     || \n \
    \          `--'   `--'"


cowsay :: String ->String
cowsay arg = 
    "  " ++ replicate len '_' ++ 
    "\n"++"< "++ arg ++" >\n" ++
    "  "++ replicate len '-' ++"\n" 
    ++ cow where len = length arg
   


    
main:: IO()
main = do
    args <- getArgs
    putStrLn(cowsay (unwords args))
  