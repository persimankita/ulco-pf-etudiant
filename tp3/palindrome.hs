{-Fonction permettant de savoir si une liste est un palindrome-}
-------------------------------------------------------------------------
palindrome:: String ->Bool
palindrome p = p == reverse p
---------------------------------------------------------------------
{-Fonction principale-}
-------------------------------------------------------------------

main :: IO()

main = do
    print(palindrome "persi")
    print(palindrome "radar")
