{-Fonction permettant de connaitre le premier élément d'une liste-}
-------------------------------------------------------------------
myFst :: (a,b) -> a
myFst (x,_) = x
------------------------------------------------------------
{-Fonction permettant de connaitre le dernier élément d'une liste-}
---------------------------------------------------------------------
mySnd :: (a,b) -> b
mySnd (_,p) = p
------------------------------------------------------------
{-Même fontion Fst avec 3 éléments-}
------------------------------------------------------------
myFst3 :: (a,b,c) -> a
myFst3 (x,_,_) = x
------------------------------------------------------------
{-Fonction principale-}
------------------------------------------------------------
main :: IO()
main = do
    print(myFst ("foo", 1))
    print(mySnd ("foo", 1))
    print(myFst3 ("foo", 1, 'a'))
