{-Fonction permettant d'afficher un message selon la taille de la liste-}
--------------------------------------------------------------------------
fuzzyLength:: [a] -> String
fuzzyLength [] = "empty"
fuzzyLength[a] = "one"
fuzzyLength[a,b] = "two"
fuzzyLength[a,b,c] = "three"
fuzzyLength _ = "many"
---------------------------------------------------------------------------
{-Fonction retournant la tête de la liste-}
----------------------------------------------------------------------------
safeHead:: [a] -> Maybe a
safeHead [] = Nothing
safeHead (x:xs) = Just x
-----------------------------------------------------------------------------
{-Fontion retournant la queue de la liste-}
------------------------------------------------------------------------------
safeTail:: [a] -> Maybe [a]
safeTail [] = Nothing
safeTail (_:ab) = Just ab
-------------------------------------------------------------------------------
{-Fonction principale-}
-------------------------------------------------------------------------------
main:: IO()
main = do
------------------------------------------------------------------
    print(fuzzyLength[])
    print(fuzzyLength[1])
    print(fuzzyLength["persi","albry"])
    print(fuzzyLength["Mankita","persi","albry"])
    print(fuzzyLength["MANKITA","KOULOUBOUKA","persi","albry"])
    print(fuzzyLength [1..4])
--------------------------------------------------------------
    print(safeHead "")
    print(safeHead "foobar")
---------------------------------------------------------------
    print(safeTail "")
    print(safeTail "foobar")
