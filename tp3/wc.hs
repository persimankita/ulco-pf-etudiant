import System.IO
import System.Environment 
-------------------------------------------------------------------------------------------------------------------------
{-programme qui lit un fichier dont le nom est passé en argument de ligne de commande et affiche son nombre de lignes-}--|
-------------------------------------------------------------------------------------------------------------------------
wc:: String->(Int,Int,Int,Int)
wc input = (nl,nm,nc,ns)
    where
        nl = length (lines input)
        nm = length (words input)
        nc = length input
        ns = length(concat(words input))
-------------------------------------------------------------------------------------------------------------------------
{-programme principal-}                                                                                                --|
-------------------------------------------------------------------------------------------------------------------------
main:: IO()
main = do
    args <- getArgs
    case args of
        [filename] -> do
            content<- readFile filename
            let(nl,nm,nc,ns) = wc content
            putStrLn("Lines :"++show nl)
            putStrLn("Words :"++ show nm)
            putStrLn("Characters :"++ show nc)
            putStrLn("Characters (no ws) :"++ show ns)
        _ -> putStrLn usage

usage:: String
usage = "usage <file>"
