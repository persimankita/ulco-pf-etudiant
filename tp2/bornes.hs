borneEtFormate1:: Double -> String

borneEtFormate1 args = show args ++ " -> " ++ show val2
    where
        val2 = if args <0
            then 0
            else if args > 1
                then 1
                else args

borneEtFormate2:: Double -> String

borneEtFormate2 args = show args ++ " -> " ++ show val2
    where
        val2 | args < 0 = 0
             | args > 1 = 1
             | otherwise = args

main::IO()
main = do
    putStrLn(borneEtFormate1 (-1.2))
    putStrLn(borneEtFormate1 0.2)

    putStrLn(borneEtFormate2 (1.2))
    putStrLn(borneEtFormate2 0.8)
