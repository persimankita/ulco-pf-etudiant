safeSqrt :: Double -> Maybe Double
safeSqrt n
    |n<0 = Nothing
    |otherwise = Just (sqrt n)

formatedSqrt::Double -> String
formatedSqrt n = "sqrt ("++ show n ++") = " ++ show result
    where result = case safeSqrt n of
            Nothing -> "is not difined"
            _ -> result

main:: IO()
main = do
    print(formatedSqrt 10)
    print(formatedSqrt (-15))
    print(formatedSqrt 100)