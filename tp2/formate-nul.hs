formateNull :: Int -> String
formateNull args = show args ++ " est " ++ fmt args
    where
        fmt x = case x of
            0 -> "Null"
            _ -> "Non null"

formateNull2 :: Int -> String
formateNull2 args = show args ++ " est " ++ fmt args
    where
        fmt 0 = "Null"
        fmt _ = "Non null"

main :: IO()
main = do
    print(formateNull 10)
    print(formateNull 0)
    print(formateNull2 30)
    print(formateNull2 0)