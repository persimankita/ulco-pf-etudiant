xorFiltrage :: Bool -> Bool -> Bool
xorFiltrage False False = False
xorFiltrage False True = True
xorFiltrage True False = True
xorFiltrage True True = False

xorIf:: Bool -> Bool -> Bool
xorIf x y = 
    if x == y then False else True

xorCaseOf:: Bool -> Bool ->Bool
xorCaseOf x y = case (x,y) of
                    (True,True)->False
                    (False,False)->False
                    (True,False)-> True
                    (False,True)->True

xorGuard:: Bool -> Bool -> Bool
xorGuard x y 
    |x && (not y) = True
    |y && (not x) = True
    |otherwise = False

xorBool:: Bool -> Bool ->Bool
xorBool x y = x && (not y) || y && (not x)

testXor :: (Bool -> Bool ->Bool)->Bool
testXor a =
    a True True == False &&
    a False False == False &&
    a True False == True &&
    a False True == True

main:: IO()
main = do
    putStrLn("--Test XorBool--")
    print(testXor xorBool)
    putStrLn("--Test XorIf--")
    print(testXor xorIf)
    putStrLn("--Test XorGuard--")
    print(testXor xorGuard)
    putStrLn("--Test XorCaseOf--")
    print(testXor xorCaseOf)
    putStrLn("--Test XorFiltrage--")
    print(testXor xorFiltrage)
  
