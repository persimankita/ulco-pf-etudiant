
formaterParite:: Integer -> String

formaterParite args =
    if even args then "pair" else "impair"

formaterSigne :: Integer -> String
formaterSigne nombre= 
    if nombre == 0
        then "nul"
        else if nombre < 0
            then "Negatif"
            else
                "Positif"
    
main :: IO()
main = do
    print(formaterParite 32)
    print(formaterSigne (-15))
    print(formaterSigne 0)