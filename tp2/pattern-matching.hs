fibo:: Integer -> Integer
fibo 0 = 0
fibo 1 = 1
fibo n = fibo (n-1) + fibo (n-2)

main :: IO()
main = do
    print(fibo 0)
    print(fibo 1)
    print(fibo 2)
    print(fibo 3)
    print(fibo 10)