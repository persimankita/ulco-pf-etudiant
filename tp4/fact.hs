fact :: Int -> Int
fact 0 = 1
fact 1 = 1
fact a = a * fact (a-1)

factTco:: Int -> Int -> Int
factTco 0 acc = acc
factTco a acc = factTco (a-1) (acc*a)


main :: IO()
main = do
    print(fact 2)
    print(fact 5)

    print(factTco 2 1)
    print(factTco 5 1)