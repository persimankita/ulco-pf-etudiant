

mylast:: String -> Char
mylast [] =  error "Error liste vide"
mylast [x] = x
mylast (_:xs) = mylast xs

myinit::String -> String
myinit [] = error "Erreur, liste vide"
myinit [x] = []
myinit (x:xs) = x : myinit xs

myreplicate:: Int -> String -> String
myreplicate 1 val = val
myreplicate entier val = val ++ myreplicate (entier - 1) val

mytake :: Int -> String -> String
mytake entier []= error "Erreur,la liste est vide"
mytake 0 val = ""
mytake entier (x:xs) = [x] ++ mytake (entier-1) xs 

mydrop :: Int -> String -> String
mydrop entier [] = error "Erreur, liste vide"
mydrop 0 val = val
mydrop entier (x:xs) = mydrop (entier-1) xs
             

main :: IO()
main = do

    putStrLn "My Last Foo Bar"
    print(mylast "Foo Bar")

    --putStrLn "liste vide"
    --print(mylast "")

    putStrLn "My Init Persi"
    print(myinit "Persi")

    putStrLn "My Replicate 'f'"
    print(myreplicate 4 "f")

    putStrLn "mytake 'foobar'"
    print(mytake 3 "foobar")

    putStrLn "Mydrop 'foobar'"
    print(mydrop 3 "foobar")
