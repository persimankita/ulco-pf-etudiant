mulListe:: [Int]-> Int -> [Int]
mulListe [] _ = []
mulListe (x:xs) n = x*n : mulListe xs n

selecliste :: (Int, Int)-> [Int] ->[Int]
selecliste (a,b) [] = []
selecliste (a,b) (x:xs) = if x >= a && x <=b
    then
        x:selecliste(a,b) xs
    else selecliste (a,b) xs


sumListe:: [Int] -> Int
sumListe []= 0
sumListe (x:xs) = x + sumListe xs

main:: IO()
main = do
    putStrLn "Multiplication de liste"
    print( mulListe [1,2,3,4] 5)
    putStrLn "selectListe"
    print(selecliste(8,36)[2,3,56,53,32,29] )
    putStrLn "sum"
    print(sumListe [10,9,1,5,3,2])

