import Data.Char
toUpperString:: String -> String
toUpperString []= []
toUpperString (x:xs) = [toUpper x ] ++ toUpperString xs

onlyLetters::String -> String
onlyLetters [] = ""
onlyLetters (x:xs) = 
    if isLetter x
        then
            [x] ++ ođnlyLetters xs
        else
            "" ++ onlyLetters xs

main:: IO()
main = do
    putStrLn (toUpperString "pangolin")

    putStrLn (onlyLetters "")
    putStrLn (onlyLetters "Le monde est mechant")
    putStrLn (onlyLetters "123 LG 45")