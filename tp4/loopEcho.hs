loopEcho ::Int -> IO String
loopEcho 0 = return "loop terminated"
loopEcho x = do
    a <- getLine
    if a == "" 
        then
        return "empty line!"
        else do
            putStrLn a
            loopEcho (x-1)
                    


main :: IO()
main = do
    status <- loopEcho 3
    putStrLn status