import System.Environment

fiboNative :: Int -> Int
fiboNative 0 = 0
fiboNative 1 = 1
fiboNative n = fiboNative (n-1) + fiboNative (n-2)


fiboTco ::Int ->Int

fiboTco n = go n 1 0
    where
        go 0 _ a2 = a2
        go x a1 a2 = go(x-1) (a1+a2) a1

main :: IO()
main = do
{-    print(fiboNative 2)
    print(fiboNative 3)
    print(fiboNative 4)
    print(fiboNative 8)

    putStrLn "Terminal :"


    print(fiboTco 2)
    print(fiboTco 3)
    print(fiboTco 4)
    print(fiboTco 8)
-}
    args <- getArgs
    case args of--Affichage console pour le choix de la fonction à utiliser
        ["naive",nstr] -> print(fiboNative (read nstr)) --Mot naive pour utiliser la première fonction de fibonacci
        ["tco",nstr] -> print(fiboTco (read nstr))
        _ -> putStrLn "usage : <naive|tco> <n>" --En cas d'oublie en précisant le paramètre ou la fonction à utiliser,afficher le message

