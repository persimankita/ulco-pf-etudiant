import Data.List
type Vector = [Char]
type Matrix = [Vector]
operations:: [(Matrix ->Vector, Matrix->Matrix)]
operations = cycle
    [(head,transpose . tail),
    (last,transpose .init),
    (reverse . last, transpose . init),
    (reverse .head, transpose .tail)]

spiral :: Matrix -> Vector
spiral m0 = go m0 operations
    where
        go [] _ = []
        go mi ((op,er):ration) = op mi ++ go (er mi) ration
        go _ [] = error ""
----------------------------------------------------
--Matrice
----------------------------------------------------
m::[[Char]]
m = [
    ['a','b','c','d'],
    ['e','f','g','h'],
    ['i','j','k','l'],
    ['m','n','o','p']
    ]
----------------------------------------------------

-----------------------------------------------------
main :: IO ()
main = do
-------------------------------------------------
--Affichage-

    print m
    print(spiral m)
    
-----------------------------------------------------



