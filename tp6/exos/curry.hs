-----------------------------------------------------------------------------
{-
Fonction qui prend un tuplue et retourne la liste d'entier qui les sépare
-}
----------------------------------------------------------------------------
myRangeTumple1:: (Int,Int) -> [Int]
myRangeTumple1 (x,y) =  [x..y]
---------------------------------------------------------------------------------
{-
Même fonction mais ne prenant qu'un seul paramètre qui retourne une liste d'entiers en 0 et ce nombre
-}
-----------------------------------------------------------------------------------
myRangeTumple2::Int -> [Int]
myRangeTumple2 x = myRangeTumple1 (0,x)
-----------------------------------------------------------------------------------
{-
Fonction qui prend 2 éléments et retourne la liste d'entiers entre ces 2 éléments
-}
------------------------------------------------------------------------------------
myRangeCurry1:: Int -> Int ->[Int]
myRangeCurry1 x y = [x..y]
------------------------------------------------------------------------------------
{-
Même fonction mais ne prenant qu'un seul paramètre qui retourne une liste d'entiers en 0 et ce nombre
-}
-------------------------------------------------------------------------------------------------------
myRangeCurry2:: Int->[Int]
myRangeCurry2 x = myRangeCurry1 0 x
-----------------------------------------------------------------------------
{-
Fonction principale
-}
------------------------------------------------------------------------------
main :: IO()
main = do
-------------------------------------------------------------------------------
--Affichage à 2 paramètres
putStrLn "Affichage à 2 paramètres"
print(myRangeTumple1 (0,9))
----------------------------------------------------
--Affichage avec un seul paramètre
putStrLn "Affichage avec un seul paramètre"
print(myRangeCurry1 0 8)