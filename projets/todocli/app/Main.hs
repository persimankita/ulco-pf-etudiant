import Text.Read
import System.Environment
import System.IO

type Task = (Int, Bool, String)--Taches à effectuer,Ainsi que son statut
type Todo = (Int, [Task])--Contenu de la liste des taches
--------------------------------------------
{-Fonction permettant d'afficher les taches-}
----------------------------------------------
printAll::Todo -> IO()
printAll todo = mapM_ print (snd todo)
-----------------------------------------------
{-Fonction permettant d'afficher les taches à faire-}
printTodo:: Todo -> IO()
printTodo todo= do
    mapM_ (putStrLn.printState) (snd todo)
--    print todo
--    
    
-------------------------------------------------------------
{-Fonction permettant d'afficher les taches selon un 
certain affichage et fonction de filtre-}
-------------------------------------------------------------
--tFilter::[Task]->Bool->String
--tFilter liste etat = filter (\(_,state,_) -> state==etat) liste
printState::Task->String--Affichage selon l'état
printState (num,state,strng) = do
    if state then
        "[X] "
    else 
        "[-]"
    ++show num++". "++strng

-------------------------------------------------------------
{-Fonction permettant d'afficher les taches déjà exécutées-}
-------------------------------------------------------------
printStateDone::Task -> String
printStateDone (num,state,strng) = do
    if state then
        ""
    else "[-] "++show num++". "++strng

printDone::Todo ->IO()
printDone  todo= mapM_ (putStrLn.printStateDone) (snd todo)
------------------------------------------------------------
{-Fonction permettant d'ajouter des taches-}
    
--------------------------------------------
{-Fonction permettant d'afficher le menu-}
--------------------------------------------
menu::IO()
menu = do
    putStrLn "usage:"
    putStrLn "\t print"
    putStrLn "\t print todo"
    putStrLn"\t print done"
    putStrLn "\t add <string>"
    putStrLn "\t do <int>"
    putStrLn "\t undo <int>"
    putStrLn "\t del <int>"
    putStrLn "\t exit"
---------------------------------------------------------
{-Fonction permettant d'ajouter des éléments au todo-}
--------------------------------------------------------
--addFile::String->IO()
--addFile obj = appendFile "tasks.txt" obj

addTodo::String->IO Task
addTodo obj = appendFile "tasks.txt" (_,False,obj)
-------------------------------------------------
{-Fonction permettant de sortir de la section-}
------------------------------------------------
exitFile:: IO()
exitFile = putStrLn ""
------------------------------------------------------------
{-Fonction permettant de sélectionner les éléments du menu-}
------------------------------------------------------------
selection::String-> Todo->IO()
selection sel todo= do
    case sel of
        "print" -> printAll todo
        "print todo" -> printTodo todo
        "print dont" ->printDone todo
        "add <string>" -> addFile
        "exit"-> exitFile
        _ -> menu


---------------------------------------------------
{-Fonction principale-}
-------------------------------------------------
main :: IO ()
main = do
    args <-getArgs
    case args of
        [filename] -> do
            argum <-getLine
--            selection argum  (read filename)
            addFile argum
        _ -> do
            putStrLn usage
            menu


---------------------------------------------------
{-fonction qui affiche un message si aucun fichier
 n'est passé en paramètre-}
 ---------------------------------------------------
usage::String
usage = "usage <filename>"
