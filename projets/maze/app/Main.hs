import qualified Data.Vector as V

import Control.Monad (forM_)
import System.Environment
import System.IO

type Maze = V.Vector Char


mazeDisplay :: Int -> Int -> Int ->Int ->  Maze -> IO()
mazeDisplay h c xi yi maze = do
    forM_ [0..h-1] $ \i -> do
        forM_ [0..c-1] $ \j -> do
            let pos = i*c+j
            let car = maze V.!pos
            if (pos ==xi*c+yi && [car] == " ")
                    then 
                        putStr "@"
            else do
                if [car]=="@" then putStr "@"
                else putStr [car]
        putStr "\n"

exitMaze :: Int -> Int->Int-> Maze -> Bool
exitMaze c xi yi maze = do 
    let pos=xi*c+yi
    let car = maze V.!pos
    if [car]=="?" then  True
    else False

loseMaze :: Int -> Int->Int-> Maze -> Bool
loseMaze c xi yi maze = do 
    let pos=xi*c+yi
    let car = maze V.!pos
    if [car]=="+" then  True
    else False

runMaze :: Int -> Int -> Int -> Int -> Maze -> IO()
runMaze h c hor cor maze = do
    putStrLn "\npress a key..."
    x <- getChar
    putStrLn ("\nyou pressed: " ++ [x])

    case x of 
        'g' ->do
            mazeDisplay h c hor (cor-1) maze 
            if exitMaze c hor (cor-1) maze then putStrLn "You Win!!!!!"
            else if loseMaze c hor (cor-1) maze then putStrLn "You lose !!!"
            else runMaze h c hor (cor-1) maze 
        'd' ->do
            mazeDisplay h c hor (cor+1) maze
            if exitMaze c hor (cor+1) maze then putStrLn "You Win!!!!!"
            else if loseMaze c hor (cor+1) maze then putStrLn "You lose !!!"
            else runMaze h c hor (cor+1) maze 
        'h' -> do
            mazeDisplay h c (hor-1) cor maze
            if exitMaze c (hor-1) cor maze then putStrLn "You Win!!!!!"
            else if loseMaze c (hor-1) cor maze then putStrLn "You lose !!!"
            else runMaze h c (hor-1) cor maze 
        'b' ->do 
                mazeDisplay h c (hor+1) cor maze
                if exitMaze c (hor+1) cor maze then putStrLn "You Win!!!!!"
                else if loseMaze c (hor+1) cor maze then putStrLn "You lose !!!"
                else runMaze h c (hor+1) cor maze 
        _ -> runMaze h c hor cor maze

   

main :: IO ()
main = do
    hSetBuffering stdin NoBuffering
    file <- readFile "data/maze1.txt"
    let (infoLine:mazeLines) = lines file
        maze = V.fromList (concat mazeLines)
    print infoLine
    let ligne = words infoLine
    let h=read(ligne !! 0)::Int
    let c=read(ligne !! 1)::Int
    let xi=read(ligne !! 2)::Int
    let yi=read(ligne !! 3)::Int
    mazeDisplay h c xi yi maze
    runMaze h c xi yi maze
  